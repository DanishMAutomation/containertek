## Danish Container Test
This is the submission for the containerised version of the techtest.

### File structure
```
.
├── Dockerfile-loadbalancer
├── Dockerfile-nginx
├── README.md
├── docker-compose.test.yml
├── docker-compose.yml
├── files
│   ├── html
│   │   └── index.html
│   ├── loadbalancer
│   │   └── default
│   ├── nginx.conf
│   └── webserver
│       └── default
├── goss
│   └── goss.yaml
└── tests.sh
```


#### Prerequisites

* Install docker:
```
brew install docker
```
* Install docker-compose:
```
brew install docker-compose
```
* Install goss and dgoss tool (MacOS instructions):

```
# Install dgoss
curl -L https://raw.githubusercontent.com/aelsabbahy/goss/master/extras/dgoss/dgoss -o /usr/local/bin/dgoss
chmod +rx /usr/local/bin/dgoss

# Download goss to your preferred location
curl -L https://github.com/aelsabbahy/goss/releases/download/v0.3.6/goss-linux-amd64 -o ~/Downloads/goss-linux-amd64

# Set your GOSS_PATH to the above location
export GOSS_PATH=~/Downloads/goss-linux-amd64
```


#### Deployment
1. Clone the repository with relevant files:
```
git clone git@bitbucket.org:DanishMAutomation/containertek.git && cd containertek
```
2. Create infrastructure to run as daemon:
```
docker-compose up -d
```
3. Run automated tests:
```
./tests.sh
```

#### Cleanup
To cleanup all running containers:
```
docker kill $(docker ps -q) && docker rm $(docker ps -a -q)
```
